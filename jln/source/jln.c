#include "jln.h"
#include <assert.h>
#include "containers.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	FILE *fptr = fopen("./test.jln", "rb");
	if (fptr == NULL) {
		puts("failed to open file");
		goto e1;
	}

	fseek(fptr, 0, SEEK_END);
	long fsize = ftell(fptr);
	rewind(fptr);

	char *buffer = malloc(fsize + 1);
	fread(buffer, sizeof(char), fsize, fptr);
	buffer[fsize] = 0;

	puts(buffer);
	free(buffer);
	fclose(fptr);

	JLNQuery arr[3];
	printf("%d\n", ParseJLNQuery("[1][2][3]", arr, 3));
	printf("%.*s\n", arr[1].len, arr[1].ptr);

	getchar();
	return 0;

e1:	getchar();
	return -1;
}

void JLNValue_Init(JLNValue *self, char *ptr, ENUM(jln_type) type) {
	self->ptr = ptr;
	self->type = type;
}

typedef uint8_t ENUM(jln_state);
#define ENUM_JLN_STATE_HALT 0
#define ENUM_JLN_STATE_START 1
#define ENUM_JLN_STATE_KEY 2
#define ENUM_JLN_STATE_INDEX 3

int ParseJLNQuery(cstr *query, JLNQuery *arr, int cap) {
	char ch;
	int index = 0;
	ENUM(jln_state) state = ENUM_JLN_STATE_START;

while (state != ENUM_JLN_STATE_HALT) {
	ch = *query;

	// extra checks but less states
	// ugly hackery and not robust
	// check your string inputs
	// no bounds check for JLNQuery *arr
	switch (state) {
	case ENUM_JLN_STATE_START:
		if (ch == '.') {
			state = ENUM_JLN_STATE_KEY;
			arr[index].ptr = query + 1;
			arr[index].len = 0;
			arr[index].type = ENUM_JLN_QUERY_TYPE_KEY;
			query++;
		} else if (ch == '[') {
			state = ENUM_JLN_STATE_INDEX;
			arr[index].ptr = query + 1;
			arr[index].len = 0;
			arr[index].type = ENUM_JLN_QUERY_TYPE_INDEX;
			query++;
		} else if (ch == '\0') {
			state = ENUM_JLN_STATE_HALT;
		} else {
			state = ENUM_JLN_STATE_HALT;
			index = -1;
		}
		break;
	case ENUM_JLN_STATE_KEY:
		if (ch == '.' || ch == '[' ||  ch == '\0') {
			state = ENUM_JLN_STATE_START;
			index++;
		} else {
			arr[index].len++;
			query++;
		}
		break;
	case ENUM_JLN_STATE_INDEX:
		if (ch == ']') {
			state = ENUM_JLN_STATE_START;
			index++;
			query++;
		} else if (isdigit(ch)) {
			arr[index].len++;
			query++;
		} else {
			state = ENUM_JLN_STATE_HALT;
			index = -2;
		}
		break;
	default:
		state = ENUM_JLN_STATE_HALT;
		break;
	}
}

	return index;
}

retcode JLNValue_Query(JLNValue *self, JLNQuery *arr, umax len) {
	retcode code = 0;
	umax i = 0;
	for (; i < len && code >= 0; i++) {
		switch (arr[i].type) {
		case ENUM_JLN_QUERY_TYPE_KEY:
			break;
		case ENUM_JLN_QUERY_TYPE_INDEX:
			break;
		default:
			code = -1;
			break;
		}
	}

	return code;
}





