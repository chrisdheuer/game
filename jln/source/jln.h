#ifndef JLN_H
#define JLN_H
#include "defs.h"

typedef uint8_t ENUM(jln_query_type);
#define ENUM_JLN_QUERY_TYPE_INDEX 0
#define ENUM_JLN_QUERY_TYPE_KEY 1

typedef struct {
	char *ptr;
	uint32 len;
	ENUM(jln_query_type) type;
} JLNQuery;

typedef uint8_t ENUM(jln_type);
#define ENUM_JLN_TYPE_STRING 0
#define ENUM_JLN_TYPE_ARRAY 1
#define ENUM_JLN_TYPE_MAP 2

typedef struct {
	char *ptr;
	ENUM(jln_type) type;
} JLNValue;

void JLNValue_Init(JLNValue *self, char *ptr, ENUM(jln_type) type);
retcode JLNValue_Query(JLNValue *self, JLNQuery *arr, umax len);
int ParseJLNQuery(cstr *query, JLNQuery *arr, int cap);

#endif
