#include "tests.h"
#include "allocators.h"
#include <stdio.h>

int main(int argc, char **argv) {

	if (GlobalAllocators_New(&g_alloc) < 0)
		return -1;

	printf("%zu\n", sizeof(Chunk));

	Allocator_FirstFit(g_alloc.main, 64);
	Allocator_FirstFit(g_alloc.main, 512);
	Allocator_FirstFit(g_alloc.main, 128);
	Allocator_Print(g_alloc.main);
	getchar();
	GlobalAllocators_Free(&g_alloc);
	return 0;
}
