#version 330 core

out vec3 color;

uniform vec3 filter;

void main() {
	color = filter;
}
