#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;

uniform vec3 camera;

void main() {
	gl_Position = vec4(vertexPosition_modelspace + camera, 1);
}
