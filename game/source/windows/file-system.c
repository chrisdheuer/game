#include "file-system.h"
#include <stdio.h>
#include <windows.h>

retcode File_Open(File *self, const cstr *path, flag access) {
	DWORD dwDesiredAccess = 0;
	if (access & FILE_ACCESS_READ)
		dwDesiredAccess |= GENERIC_READ;
	if (access & FILE_ACCESS_WRITE)
		dwDesiredAccess |= GENERIC_WRITE;

	*self = CreateFileA(path, dwDesiredAccess, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (*self == INVALID_HANDLE_VALUE)
		return -1;

	return 0;
}

retcode File_Close(File self) {
	if (CloseHandle(self) <= 0)
		return -1;

	return 0;
}

retcode File_Copy(File self, void *buf, umax size) {
	int bytesRead;
	if (ReadFile(self, buf, size, &bytesRead, NULL) <= 0)
		return -1;

	return 0;
}

retcode FileQuery_Init(FileQuery *self, File file) {
	if (GetFileSizeEx(file, &self->size) <= 0)
		return -1;

	return 0;
}
