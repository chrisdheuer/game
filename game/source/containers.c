#include "containers.h"
#include <assert.h>
#include <string.h>
/*
void Stack_Init(Stack *self, void *buf, umax count, umax stride) {
	self->buf = buf;
	self->end = (byte*)buf + (count * stride);
	self->top = (byte*)buf - stride;
}

retcode Stack_Push(Stack *self, void *buf, umax size) {
	if (self->top + size > self->end)
		return -1;

	self->top += size;
	memcpy(self->top, buf, size);
	return 0;
}

retcode Stack_Pushp(Stack *self, void *ptr) {
	if (self->top + sizeof(void*) > self->end)
		return -1;

	self->top += sizeof(void*);
	(*(void**)self->top) = ptr;
	return 0;
}

void *Stack_Pushe(Stack *self, umax size) {
	if (self->top + size > self->end)
		return NULL;

	self->top += size;
	return self->top;
}

void *Stack_Pop(Stack *self, umax size) {
	if (self->top < self->buf)
		return NULL;

	void *tmp = self->top;
	self->top -= size;
	return tmp;
}

umax Stack_Count(Stack *self, umax size) {
	uint i = 0;
	byte *iter = self->buf;
	for (; iter <= self->top; ++i, iter += size) {}
	return i;
}

umax Stack_Capacity(Stack *self, umax size) {
	uint i = 0;
	byte *iter = self->buf;
	for (; iter < self->end; ++i, iter += size) {}
	return i;
}

void LList_Init(LList *self, void *buf, umax size) {
	memcpy(self, buf, size);
}

void LList_Append(LList *self, LList *app) {
	for (; self->next != NULL; self = self->next) {}

	self->next = app;
}

void Array_Init(Array *self, void *buf, umax size) {
	self->end = (byte*)buf + size;
	self->last = buf;
	self->buf = buf;
}

retcode Array_Append(Array *self, void *buf, umax size) {
	if (self->last + size > self->end)
		return -1;

	memcpy(self->last, buf, size);
	self->last += size;
}
*/

void Stack_Init(Stack *self, void *buf, uint16 capacity) {
	self->start = buf;
	self->capacity = capacity;
	self->count = 0;
}

retcode Stack_PushCopy(Stack *self, void *buf, umax stride) {
	if (self->count >= self->capacity)
		return -1;

	memcpy(self->start + (stride * self->count), buf, stride);
	self->count++;
	return 0;
}

retcode Stack_Push8(Stack *self, int8 val) {
	if (self->count >= self->capacity)
		return -1;

	*(int8*)(self->start + (sizeof(int8) * self->count)) = val;
	self->count++;
	return 0;
}

void SLList_Append(SLList *self, SLList *node) {
	for (; self->next != NULL; self = self->next) {}
	self->next = node;
	node->next = NULL;
}
