#ifndef STAGES_H
#define STAGES_H
#include "allocators.h"
#include "containers.h"
#include "defs.h"
#include "events.h"
#include <SDL.h>

typedef SDL_Event Input;

typedef struct Stage Stage;
typedef retcode (*StageMethod)(Stage*);
typedef void (*InputHandler)(Input*);

struct Stage {
	StageMethod ctor;
	StageMethod dtor;
	STACK(InputHandler) inputHandlers;
	StackAllocator eventBuffer;
	LLIST(EventListener*) *listenersHeads[NUM_EVENTS];
	POOLALLOCATOR(EventListener) eventListeners;
};

typedef struct {
	StageMethod ctor;
	StageMethod dtor;
	uint32 eventBufferSize;
	uint32 inputHandlersCount;
	uint32 eventListenersCount;
} StageConfig;

void StageConfig_Init(StageConfig *self, StageMethod ctor, StageMethod dtor,
	uint32 eventBufferSize, uint32 inputHandlersCount, uint32 eventListenersCount);

retcode Stage_Load(Stage *self, StageConfig *config);
retcode Stage_Unload(Stage *self);
retcode Stage_Listen(Stage *self, event_type type, EntityRef entity, EventHandler handler);

#endif