#include "events.h"
#include "game.h"
#include "stages.h"
#include <stdio.h>
#include <string.h>

void StageConfig_Init(StageConfig *self, StageMethod ctor, StageMethod dtor,
	uint32 eventBufferSize, uint32 inputHandlersCount, uint32 eventListenersCount)
{
	self->ctor = ctor;
	self->dtor = dtor;
	self->eventBufferSize = eventBufferSize;
	self->inputHandlersCount = inputHandlersCount;
	self->eventListenersCount = eventListenersCount;
}

retcode Stage_Load(Stage *self, StageConfig *config) {
	retcode code;
	void *inputHandlers = ChunkAllocator_FirstFit(g_vars.alloc, config->inputHandlersCount * sizeof(InputHandler));
	if (inputHandlers == NULL) { code = -1; goto e1; }
	STACK_INIT(InputHandler, self->inputHandlers, config->inputHandlersCount, inputHandlers);

	void *eventBuffer = ChunkAllocator_FirstFit(g_vars.alloc, config->eventBufferSize);
	if (eventBuffer == NULL) { code = -2; goto e2; }
	StackAllocator_Init(&self->eventBuffer, eventBuffer, config->eventBufferSize);

	uint32 poolSize = (config->eventListenersCount * sizeof(EventListener)) + (config->eventListenersCount * sizeof(boolean));
	void *eventListeners = ChunkAllocator_FirstFit(g_vars.alloc, poolSize);
	POOLALLOCATOR_INIT(EventListener, self->eventListeners, config->eventListenersCount, eventListeners, poolSize);

	self->ctor = config->ctor;
	self->dtor = config->dtor;
	memset(self->listenersHeads, NULL, sizeof(LList*) * NUM_EVENTS);

	if (self->ctor(self) < 0) { code = -4; goto e4; }

	return 0;

e4:	ChunkAllocator_Free(g_vars.alloc, eventListeners);
e3:	ChunkAllocator_Free(g_vars.alloc, eventBuffer);
e2:	ChunkAllocator_Free(g_vars.alloc, inputHandlers);
e1:	return code;
}

retcode Stage_Unload(Stage *self) {
	retcode code = 0;
	if (self->dtor(self) < 0) { code = -1; }

	ChunkAllocator_Free(g_vars.alloc, self->inputHandlers.buf);
	ChunkAllocator_Free(g_vars.alloc, self->eventBuffer.buf);
	ChunkAllocator_Free(g_vars.alloc, self->eventListeners.buf);

	return code;
	return 0;
}

retcode Stage_Listen(Stage *self, event_type type, EntityRef entity, EventHandler handler) {
	EventListener listenerCopy;
	EventListener_Init(&listenerCopy, entity, handler);

	LList *listener = PoolAllocator_Alloc(&self->eventListeners, sizeof(EventListener));
	LList_Init(listener, &listenerCopy, sizeof(EventListener));
	LList *iter = self->listenersHeads[type];
	if (iter == NULL) {
		EventListener *thing = (EventListener*)listener;
		self->listenersHeads[type] = listener;
		return 0;
	}
	else {
		LList_Append(self->listenersHeads[type], listener);
		return 0;
	}
}


