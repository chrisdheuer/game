#ifndef GAME_H
#define GAME_H
#include "allocators.h"
#include "maths.h"
#include "stages.h"

typedef struct {
	ChunkAllocator alloc;
	Stage stage;
	StackAllocator scratch;
	boolean isRunning;
	vec3f camera;
	vec3f triangle[3];
} Globals;

Globals g_vars;

#endif