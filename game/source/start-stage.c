#include "jln.h"
#include "game.h"
#include "start-stage.h""
#include <stdio.h>

#define START_STAGE_INPUT_HANDLERS_COUNT 10
#define START_STAGE_EVENT_BUFFER_SIZE 256
#define START_STAGE_EVENT_LISTENERS_COUNT 10

void StartStageConfig_Init(StageConfig *self) {
	StageConfig_Init(self, StartStage_Ctor, StartStage_Dtor,
		START_STAGE_INPUT_HANDLERS_COUNT, START_STAGE_EVENT_BUFFER_SIZE,
		START_STAGE_EVENT_LISTENERS_COUNT);
}

void ExitHandler(Entity *entity, Event *evnt) {
	g_vars.isRunning = FALSE;
}

void MenuHandler(Entity *entity, Event *evnt) {
	g_vars.camera[1] += 0.1f;
}

void MenuHandler2(Entity *entity, Event *evnt) {
	g_vars.camera[1] -= 0.1f;
}

void MenuHandler3(Entity *entity, Event *evnt) {
	g_vars.camera[0] -= 0.1f;
}

void MenuHandler4(Entity *entity, Event *evnt) {
	g_vars.camera[0] += 0.1f;
}
retcode StartStage_Ctor(Stage *self) {
	InputHandler handler = StartStage_InputHandler;
	//Stack_Push(&self->inputHandlers, &handler, sizeof(InputHandler));
	STACK_PUSHP(InputHandler, self->inputHandlers, StartStage_InputHandler);
	Stage_Listen(self, EVENT_TYPE_MENU_UP, NULL, MenuHandler);
	Stage_Listen(self, EVENT_TYPE_MENU_DOWN, NULL, MenuHandler2);
	Stage_Listen(self, EVENT_TYPE_MENU_LEFT, NULL, MenuHandler3);
	Stage_Listen(self, EVENT_TYPE_MENU_RIGHT, NULL, MenuHandler4);
	Stage_Listen(self, EVENT_TYPE_EXIT, NULL, ExitHandler);
	return 0;
}

retcode StartStage_Dtor(Stage *self) {
	return 0;
}

void StartStage_InputHandler(Input *input) {
	Event *evnt = StackAllocator_Alloc(&g_vars.stage.eventBuffer, sizeof(Event));
	switch (input->type) {
	case SDL_KEYDOWN:
		switch (input->key.keysym.sym) {
		case SDLK_UP:
			evnt->type = EVENT_TYPE_MENU_UP;
			break;
		case SDLK_DOWN:
			evnt->type = EVENT_TYPE_MENU_DOWN;
			break;
		case SDLK_LEFT:
			evnt->type = EVENT_TYPE_MENU_LEFT;
			break;
		case SDLK_RIGHT:
			evnt->type = EVENT_TYPE_MENU_RIGHT;
			break;
		case SDLK_ESCAPE:
			evnt->type = EVENT_TYPE_EXIT;
			break;
		default:
			evnt->type = EVENT_NOTHING;
			break;
		}
		break;
	default:
		evnt->type = EVENT_NOTHING;
		break;
	}
}
