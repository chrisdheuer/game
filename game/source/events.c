#include "events.h"

void EventListener_Init(EventListener *self, EntityRef entity, EventHandler handler) {
	self->link.next = NULL;
	self->entity = entity;
	self->handler = handler;
}
