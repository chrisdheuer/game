#include "maths.h"

void vec3f_add(vec3f self, vec3f other) {
	self[0] += other[0];
	self[1] += other[1];
	self[2] += other[2];
}

void vec3f_init(vec3f self, float x, float y, float z) {
	self[0] = x;
	self[1] = y;
	self[2] = z;
}

void vec3fv_init(vec3f *self, umax len, vec3f *other) {
	umax i = 0;
	for (; i < len; ++i) {
		self[i][0] = other[i][0];
		self[i][1] = other[i][1];
		self[i][2] = other[i][2];
	}
}
