#ifndef START_STAGE_H
#define START_STAGE_H
#include "stages.h"

void StartStageConfig_Init(StageConfig *self);
retcode StartStage_Ctor(Stage *self);
retcode StartStage_Dtor(Stage *self);
void StartStage_InputHandler(Input *input);
#endif