#ifndef ALLOCATORS_H
#define ALLOCATORS_H
#include "defs.h"

typedef struct {
	uint32 size;
	boolean isFree;
	byte padding[3];
} Chunk;

typedef Chunk* ChunkAllocator;

void ChunkAllocator_Init(ChunkAllocator *self, void *buf, uint32 size);
void *ChunkAllocator_FirstFit(ChunkAllocator self, uint32 size);
void ChunkAllocator_Free(ChunkAllocator self, void *addr);

typedef struct {
	void **last;
	byte *brk;
	void **buf;
} StackAllocator;

void StackAllocator_Init(StackAllocator *self, void *buf, uint32 size);
void *StackAllocator_Alloc(StackAllocator *self, uint32 size);
void *StackAllocator_Pop(StackAllocator *self);
void *StackAllocator_Peek(StackAllocator *self);
void StackAllocator_Clear(StackAllocator *self);

typedef struct {
	uint32 count;
	boolean *buf;
} PoolAllocator;

#define POOLALLOCATOR(type) PoolAllocator
#define POOLALLOCATOR_INIT(type, self, count, buf, size) PoolAllocator_Init(&self, buf, size, count)
void PoolAllocator_Init(PoolAllocator *self, void *buf, uint32 size, uint32 count);
void *PoolAllocator_Alloc(PoolAllocator *self, uint32 stride);
void PoolAllocator_Free(PoolAllocator *self, void *addr);

#ifdef DEBUG 
void Allocator_Print(Allocator self);
#endif
#endif
