#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H
#include "defs.h"

#define FILE_ACCESS_READ 1 << 0
#define FILE_ACCESS_WRITE 1 << 1

typedef void* File;

typedef struct {
	umax size;
} FileQuery;

retcode File_Open(File *self, const cstr *path, flag access);
retcode File_Close(File self);
retcode File_Copy(File self, void *buf, umax size);
retcode FileQuery_Init(FileQuery *self, File file);


#endif