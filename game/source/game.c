#include "game.h"
#include "file-system.h"
#include "maths.h"
#include "start-stage.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>

typedef struct {
	SDL_Window *window;
	SDL_GLContext *glContext;
} Game;

Game g_game;
const char *g_vshader[] = { "#version 330 core \nlayout (location = 0) in vec3 aPos; uniform vec3 cam; void main() { gl_Position = vec4(aPos + cam, 1.0); }" };
const char *g_fshader[] = { "#version 330 core \nout vec4 color; void main() { color = vec4(1.0f, 0.5f, 0.2f, 1.0f); }" };

int main(int argc, char **argv);
retcode Game_Init(Game *self);
void Game_Clean(Game *self);
void Game_Run(Game *self);

int main(int argc, char **argv) {
	if (Game_Init(&g_game) < 0)
		return -1;

	Game_Run(&g_game);
	Game_Clean(&g_game);
}

retcode Game_Init(Game *self) {
	/*
	retcode code;

	void *buf = malloc(GLOBAL_ALLOCATOR_SIZE);
	if (buf == NULL) { code = -1; goto e1; }
	ChunkAllocator_Init(&g_vars.alloc, buf, GLOBAL_ALLOCATOR_SIZE);
	
	void *scratch = ChunkAllocator_FirstFit(g_vars.alloc, SCRATCH_SIZE);
	if (scratch == NULL) { code = -2; goto e2; }
	StackAllocator_Init(&g_vars.scratch, scratch, SCRATCH_SIZE);

	if (SDL_Init(SDL_INIT_VIDEO) < 0) { code = -3; goto e1; }

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	self->window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (self->window == NULL) { code = -4; goto e3; }

	self->glContext = SDL_GL_CreateContext(self->window);
	if (self->glContext == NULL) { code = -5; goto e4; }

	glewExperimental = true;
	if (glewInit() != GLEW_OK) { code = -6; goto e4; }

	return 0;

e4:	SDL_DestroyWindow(self->window);
e3:	SDL_Quit();
e2:	free(buf);
e1:	return code;
*/
	return 0;
}

void Game_Clean(Game *self) {
	/*
	SDL_Quit();
	free(g_vars.alloc);
	*/
}

void Game_Run(Game *self) {
	/*
	StageConfig config;
	StartStageConfig_Init(&config);
	Stage_Load(&g_vars.stage, &config);

	vec3f triangle[3] = {
		{-0.25f, -0.25f, 0.0f},
		{0.0f, 0.25f, 0.0f},
		{0.25f, -0.25f, 0.0f}
	};

	printf("float: %zu\ntriangle: %zu\n", sizeof(float), sizeof(triangle));

	vec3fv_init(g_vars.triangle, 3, triangle);

	uint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vars.triangle), g_vars.triangle , GL_STATIC_DRAW);

	char msgBuf[1024] = { 0 };
	int result;
	uint vshader = glCreateShader(GL_VERTEX_SHADER);
	printf("vshader %d\n", vshader);
	glShaderSource(vshader, 1, &g_vshader, NULL);
	glCompileShader(vshader);
	glGetShaderiv(vshader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		puts("failed to compile vertex shader");
		glGetShaderInfoLog(vshader, 1024, NULL, msgBuf);
		puts(msgBuf);
	}

	uint fshader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fshader, 1, &g_fshader, NULL);
	glCompileShader(fshader);
	glGetShaderiv(fshader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		puts("failed to compile fragment shader");
		glGetShaderInfoLog(fshader, 1024, NULL, msgBuf);
		puts(msgBuf);
	}

	uint program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		puts("failed to link program");
		glGetProgramInfoLog(program, 1024, NULL, msgBuf);
		puts(msgBuf);
	}

	glUseProgram(program);

	vec3f_init(g_vars.camera, 0.0f, 0.0f, 0.0f);
	int cam = glGetUniformLocation(program, "cam");

	glClearColor(0.0f, 0.5f, 0.5f, 1.0f);

	SDL_Event event;
	Event *evnt;
	EventListener *listener = NULL;
	g_vars.isRunning = TRUE;
	while (g_vars.isRunning == TRUE) {
		glClear(GL_COLOR_BUFFER_BIT);
		glUniform3fv(cam, 1, g_vars.camera);
		glEnableVertexAttribArray(0);
		glBufferData(GL_ARRAY_BUFFER, sizeof(g_vars.triangle), g_vars.triangle , GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDisableVertexAttribArray(0);
		SDL_GL_SwapWindow(self->window);

		while (SDL_PollEvent(&event) != 0) {
			(*STACK_TOP(InputHandler, g_vars.stage.inputHandlers))(&event);
			while ((evnt = StackAllocator_Pop(&g_vars.stage.eventBuffer)) != NULL) {
				listener = g_vars.stage.listenersHeads[evnt->type];
				for (; listener != NULL; listener = (EventListener*)listener->link.next) {
					listener->handler(listener->entity, evnt);
				}
			}
		}
	}
	*/
	while (1) {}
}
