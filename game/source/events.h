#ifndef EVENTS_H
#define EVENTS_H
#include "containers.h"
#include "defs.h"
#include "entities.h"

typedef struct Event Event;
typedef void (*EventHandler)(Entity*, Event*);

typedef struct {
	LList link;
	EntityRef entity;
	EventHandler handler;
} EventListener;

void EventListener_Init(EventListener *self, EntityRef entity, EventHandler handler);

struct Event {
	event_type type;
};

#endif