#ifndef RENDERING_H
#define RENDERING_H
#include "game.h"

retcode LoadShaders(GLuint *prog, cstr *vertex, cstr *fragment);

#endif