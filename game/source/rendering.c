#include "file-system.h"
#include "rendering.h"
#include <assert.h>
#include <stdio.h>

/*
typedef GLenum shader_type;
typedef GLenum err_type;

#define ERR_TYPE_SHADER 0
#define ERR_TYPE_PROGRAM 1

inline cstr *CopyShader(cstr *path);
inline void FreeShader();
inline retcode CompileShader(uint *id, cstr *shader, shader_type type);
inline void PrintError(err_type type, uint id);
inline retcode CreateProgram(uint *prog, uint vertId, uint fragId);

retcode LoadShaders(GLuint *prog, cstr *vertex, cstr *fragment) {
	retcode code;
	err_type errtype;
	uint errid;
	GLuint vertId, fragId;
	char *buf;

	if ((buf = CopyShader(vertex)) == NULL) { code = -1; goto e1; }
	if (CompileShader(&vertId, buf, GL_VERTEX_SHADER) < 0) { puts("vertex"); errtype = ERR_TYPE_SHADER; errid = vertId; code = -2; goto e2; }
	FreeShader();

	if ((buf = CopyShader(fragment)) == NULL) { code = -3; goto e1; }
	if (CompileShader(&fragId, buf, GL_FRAGMENT_SHADER) < 0) { puts("fragment"); errtype = ERR_TYPE_SHADER; errid = fragId; code = -4; goto e2; }
	FreeShader();

	if (CreateProgram(prog, vertId, fragId) < 0) { errtype = ERR_TYPE_PROGRAM; errid = *prog; code = -5; goto e3; }
	return 0;

e3:	glDeleteShader(vertId);
	glDeleteShader(fragId);
	goto er;
e2:	FreeShader();
er:	PrintError(errtype, errid);
e1:	return code;
}

inline char *CopyShader(cstr *path) {
	File shader;
	FileQuery query;
	char *buf;
	if (File_Open(&shader, path, FILE_ACCESS_READ) < 0)
		goto e1;
	if (FileQuery_Init(&query, shader) < 0)
		goto e2;
	if ((buf = Stack_Request(g_alloc.scratch, query.size + 1)) == NULL)
		goto e2;
	if (File_Copy(shader, buf, query.size) < 0)
		goto e3;

	*(buf + query.size) = 0;
	return buf;

e3:	Stack_Release(g_alloc.scratch);
e2:	File_Close(shader);
e1:	return NULL;
}

inline void FreeShader() {
	Stack_Release(g_alloc.scratch);
}

inline retcode CompileShader(uint *id, cstr *shader, shader_type type) {
	GLint result;
	*id = glCreateShader(type);
	glShaderSource(*id, 1, &shader, NULL);
	glCompileShader(*id);
	glGetShaderiv(*id, GL_COMPILE_STATUS, &result);
	return (result == GL_TRUE) ? 0 : -1;
}

inline void PrintError(err_type type, uint id) {
	int errLen;
	char *msg;
	switch (type) {
	case ERR_TYPE_SHADER:
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &errLen);
		msg = Stack_Request(g_alloc.scratch, errLen + 1);
		glGetShaderInfoLog(id, errLen, NULL, msg);
		break;
	case ERR_TYPE_PROGRAM:
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &errLen);
		msg = Stack_Request(g_alloc.scratch, errLen + 1);
		glGetProgramInfoLog(id, errLen, NULL, msg);
		break;
	default:
		assert(true);
		return;
	}

	*(msg + errLen) = 0;
	puts(msg);
	Stack_Release(g_alloc.scratch);
}

inline retcode CreateProgram(uint *prog, uint vertId, uint fragId) {
	GLint result;
	*prog = glCreateProgram();
	glAttachShader(*prog, vertId);
	glAttachShader(*prog, fragId);
	glLinkProgram(*prog);
	glGetProgramiv(*prog, GL_LINK_STATUS, &result);
	if (result == GL_FALSE)
		return -1;
	glDetachShader(*prog, vertId);
	glDetachShader(*prog, fragId);
	glDeleteShader(vertId);
	glDeleteShader(fragId);
	return 0;
}
*/
