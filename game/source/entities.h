#ifndef ENTITIES_H
#define ENTITIES_H
#include "defs.h"

typedef struct {
	umax id;
	uint32 refCount;
	uint32 components;
} Entity;

typedef Entity* EntityRef;

#endif