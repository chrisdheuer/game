#ifndef JLN_H
#define JLN_H
#include "containers.h"
#include "defs.h"

typedef uint8_t jln_type;
#define JLN_TYPE_STRING 0
#define JLN_TYPE_ARRAY 1
#define JLN_TYPE_MAP 2

typedef struct {
	uint32 len;
	cstr arr[];
} String;

typedef struct {
	jln_type type;
} JLNValue;

typedef struct {
	JLNValue base;
} JLNString;

typedef struct {
	JLNValue base;
	uint32 count;
} JLNArray;

typedef struct {
	JLNValue base;
	uint32 count;
} JLNMap;

typedef struct JLNContext JLNContext;
typedef retcode	(*JLNTransition)(JLNContext*);

struct JLNContext {
	cstr *expr;
	char *head;
	ARRAY(byte) bln;
	STACK(JLNTransition) transitions;
	STACK(JLNArray) values;
};

retcode JLN_Compile(JLNContext *ctx);

#endif