#ifndef CONTAINERS_H
#define CONTAINERS_H
#include "defs.h"

/*
typedef struct {
	byte *end;
	byte *top;
	byte *buf;
} Stack;

void Stack_Init(Stack *self, void *buf, umax count, umax stride);
retcode Stack_Push(Stack *self, void *buf, umax size);
retcode Stack_Pushp(Stack *self, void *ptr);
void *Stack_Pushe(Stack *self, umax size);
void* Stack_Pop(Stack *self, umax size);
umax Stack_Count(Stack *self, umax size);
umax Stack_Capacity(Stack *self, umax size);

#define STACK(type) Stack
#define STACK_INIT(type, self, count, buf) Stack_Init(&self, buf, count, sizeof(type))
#define STACK_PUSH(type, self, buf) Stack_Push(&self, buf, sizeof(type))
#define STACK_PUSHP(type, self, ptr) Stack_Pushp(&self, ptr)
#define STACK_PUSHE(type, self) Stack_Pushe(&self, sizeof(type))
#define STACK_POP(type, self) Stack_Pop(&self, sizeof(type))
#define STACK_PEEK(type, self) (self.top < self.buf) ? NULL : ((type*)self.top)
#define STACK_CLEAR(type, self) stack.top = (stack.top - sizeof(type))
#define STACK_TOP(type, self) ((type*)(self.top))
#define STACK_COUNT(type, self) Stack_Count(&self, sizeof(type))
#define STACK_CAP(type, self) Stack_Capacity(&self, sizeof(type))

#define LLIST(type) LList

typedef struct LList {
	struct LList *next;
} LList;

void LList_Init(LList *self, void *buf, umax size);
void LList_Append(LList *self, LList *app);
#define LLIST_BUFFER(self) self + 1

#define ARRAY(type) Array

typedef struct {
	byte *end;
	byte *last;
	byte *buf;
} Array;

void Array_Init(Array *self, void *buf, umax size);
retcode Array_Append(Array *self, void *buf, umax size);


*/

#define CONTAINER_OF(type, ptr, member) \
	((type*)((byte*)ptr - offsetof(type, member)))


typedef struct {
	byte *start;
	uint16 capacity;
	uint16 count;
} Stack;

void Stack_Init(Stack *self, void *buf, uint16 capacity);
retcode Stack_PushCopy(Stack *self, void *buf, umax stride);
retcode Stack_Push8(Stack *self, int8 val);

#define STACK(type) \
	Stack
#define STACK_INIT(type, self, buf, capacity) \
	Stack_Init(self, buf, capacity)
#define STACK_PUSHCOPY(type, self, buf) \
	Stack_PushCopy(self, buf, sizeof(type))
#define STACK_PUSH8(type, self, val) \
	Stack_Push8(self, val)
#define STACK_TOP(type, self) \
	((type*)(self).start + (((self).count - 1) * sizeof(type)))

typedef struct SLList {
	struct SLList *next;
} SLList;

void SLList_Append(SLList *self, SLList *node);

#define SLLIST(type) \
	SLList
#define SLLIST_INIT(type, self) \
	(self).next = NULL
#define SSLIST_APPEND(type, self, node) \
	SLList_Append(self, node)
#define SLLIST_FOR_EACH(type, iter, head) \
	for (SLList *iter = head; iter != NULL; iter = iter->next)

#endif