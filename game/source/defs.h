#ifndef DEFS_H
#define DEFS_H
#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
//#include <SDL.h>

typedef size_t umax;
typedef int retcode;
typedef char cstr;
typedef uint32_t flag;
typedef uint8_t byte;
typedef unsigned uint;
typedef uint8_t uint8;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uintptr_t uintptr;
typedef uint8_t boolean;
typedef int8_t int8;

#define TRUE 1
#define FALSE 0

#define ENUM(alias) alias
void EnumAssert(int value, uint numArgs, ...);

#define EVENT_TYPE_MENU_UP 0
#define EVENT_TYPE_MENU_DOWN 1
#define EVENT_TYPE_MENU_LEFT 2
#define EVENT_TYPE_MENU_RIGHT 3
#define EVENT_TYPE_EXIT 4
#define EVENT_NOTHING 5
#define NUM_EVENTS 6

#define EVENT_TYPE_MENUMOVE 0
#define MENU_DIRECTION_UP 0


#define MEM_ALIGNMENT 8
#define GLOBAL_ALLOCATOR_SIZE 16384
#define SCRATCH_SIZE 1024

#endif
