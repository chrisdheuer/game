#include "allocators.h"
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define CHUNK_BUFFER(chunk) (byte*)(chunk + 1)
#define CHUNK_NEXT(chunk) (Chunk*)(CHUNK_BUFFER(chunk) + chunk->size)
#define IS_CHUNK_END(chunk) (chunk->size == 0) ? true : false

inline void Chunk_Init(Chunk *self, uint32 size, boolean isFree);
inline void Chunk_Split(Chunk *self, uint32 size);

void ChunkAllocator_Init(ChunkAllocator *self, void *buf, uint32 size) {
	Chunk *iter = buf;
	Chunk_Init(iter, size - sizeof(Chunk), TRUE);
	Chunk_Split(iter, iter->size - sizeof(Chunk));
	*self = buf;
}

void *ChunkAllocator_FirstFit(ChunkAllocator self, uint32 size) {
	for (Chunk *iter = self; !IS_CHUNK_END(iter); iter = CHUNK_NEXT(iter)) {
		if (iter->isFree == TRUE && iter->size >= size) {
			iter->isFree = FALSE;

			uint32 splitSize = iter->size - size;
			if (splitSize >= sizeof(Chunk) + MEM_ALIGNMENT) {
				Chunk_Split(iter, size);
			}

			return CHUNK_BUFFER(iter);
		}
	}
	return NULL;
}

void ChunkAllocator_Free(ChunkAllocator self, void *buf) {
}

void StackAllocator_Init(StackAllocator *self, void *buf, uint32 size) {
	self->brk = (byte*)buf + size;
	self->buf = buf;
	*(self->buf) = (void*)self->brk;
	self->last = buf;
}

void *StackAllocator_Alloc(StackAllocator *self, uint32 size) {
	byte *newBrk = self->brk - size;
	if (self->brk - size < (byte*)(self->last + 1))
		return NULL;

	byte *tmp = self->brk;
	self->brk -= size;
	*(++self->last) = tmp;
	return tmp;
}

void *StackAllocator_Pop(StackAllocator *self) {
	if (self->last <= self->buf)
		return NULL;

	void *tmp = *(self->last);
	--self->last;
	self->brk = tmp;
	return tmp;
}

void *StackAllocator_Peek(StackAllocator *self) {
	if (self->last <= self->buf)
		return NULL;

	return *(self->last);
}

void StackAllocator_Clear(StackAllocator *self) {
	self->last = self->buf;
	self->brk = (byte*)(*(self->buf));
}

void PoolAllocator_Init(PoolAllocator *self, void *buf, uint32 size, uint32 count) {
	self->count = count;
	self->buf = buf;
	memset(self->buf, TRUE, sizeof(boolean) * count);
}

void *PoolAllocator_Alloc(PoolAllocator *self, uint32 stride) {
	uint32 i = 0;
	boolean *iter = self->buf;
	for (; i < self->count; ++i, ++iter) {
		if (*iter == TRUE) {
			*iter = FALSE;
			return (void*)((byte*)(self->buf + self->count) + (i * stride));
		}
	}

	return NULL;
}

void PoolAllocator_Free(PoolAllocator *self, void *addr) {
}

inline void Chunk_Init(Chunk *self, uint32 size, boolean isFree) {
	self->size = size;
	self->isFree = isFree;
}

inline void Chunk_Split(Chunk *self, uint32 size) {
	Chunk_Init((Chunk*)(CHUNK_BUFFER(self) + size), self->size - (size + sizeof(Chunk)), true);
	self->size = size;
}

#ifdef DEBUG
#include <stdio.h>

void Allocator_Print(Allocator self) {
	printf("#\toffset\tsize\tfree\n");
	uint i = 0;
	uint offset = 0;
	Chunk *iter = self;
	for (; iter->size != 0; ++i, offset += iter->size + sizeof(Chunk), iter = CHUNK_NEXT(iter)) {
		printf("%d\t%d\t%d\t%d\n", i, offset, iter->size, iter->isFree);
	}
}
#endif