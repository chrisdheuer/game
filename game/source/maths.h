#ifndef MATHS_H
#define MATHS_H
#include "defs.h"

typedef float vec3f[3];

void vec3f_add(vec3f self, vec3f other);
void vec3f_init(vec3f self, float x, float y, float z);
void vec3fv_init(vec3f *self, umax len, vec3f *other);

#endif